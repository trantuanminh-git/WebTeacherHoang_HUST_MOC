const first_button_add_group = document.getElementById("addGroupItem");
const first_button_add_item = document.getElementById("addInfoItem");
const button_export_PDF = document.getElementById("exportPDF");

let infoCounter = 1;
let groupCounter = 1;

first_button_add_group.addEventListener("click", function () {
    handleEventButtonsAddGroup();
});

first_button_add_item.addEventListener("click", function () {
    handleEventButtonsAddInfo();
});

button_export_PDF.addEventListener("click", function () {
    exportPDF();
});

const handleEventButtonsAddInfo = (parentElement) => {
    console.log("AddInfo");
    const infoItem = document.createElement("div");
    infoItem.classList.add("info-item");

    const label = document.createElement("p");
    label.textContent = `Info Item ${infoCounter}`;
    label.addEventListener("dblclick", () => {
        handleEventInfoItem(label);
    });

    const input = document.createElement("input");
    input.type = "text";

    const deleteButton = document.createElement("button");
    deleteButton.textContent = "🗑️";
    // deleteButton.onclick = () => parentElement.removeChild(infoItem);

    deleteButton.addEventListener("click", function () {
        let choice = confirm(
            "Xác nhận xóa thông tin " + label.textContent + "?"
        );
        if (choice == true) {
            parentElement.removeChild(infoItem);
        }
    });

    // select type of input item
    const select = document.createElement("select");
    console.log(select);
    const textOption = document.createElement("option");
    textOption.value = "text";
    textOption.text = "Text";
    select.appendChild(textOption);

    const checkBoxOption = document.createElement("option");
    checkBoxOption.value = "checkbox";
    checkBoxOption.text = "Checkbox";
    select.appendChild(checkBoxOption);
    select.style = "margin-left: 20px; width: 50%";

    const numberOption = document.createElement("option");
    numberOption.value = "number";
    numberOption.text = "Number";
    select.appendChild(numberOption);

    select.addEventListener("change", () => {
        input.type = select.value;
    });

    // appendChild
    infoItem.appendChild(label);
    infoItem.appendChild(input);
    infoItem.appendChild(select);
    infoItem.appendChild(deleteButton);
    parentElement.appendChild(infoItem);
    infoCounter++;
};

const handleEventButtonsAddGroup = () => {
    console.log("AddGroup");
    const container = document.getElementById("info-container");
    const groupItem = document.createElement("div");
    groupItem.classList.add("group");

    const groupHeader = document.createElement("div");
    groupHeader.classList.add("group-header");

    const groupTitle = document.createElement("h2");
    groupTitle.textContent = `Thong tin ${groupCounter}_20215182`;
    groupTitle.addEventListener("dblclick", () => {
        console.log("group clicked");
        handleEventGroupTitle(groupTitle);
    });

    const addInfoButton = document.createElement("button");
    addInfoButton.textContent = "Add Info Item";
    addInfoButton.onclick = () => handleEventButtonsAddInfo(groupItem);

    const addGroupButton = document.createElement("button");
    addGroupButton.textContent = "Add Group Item";
    addGroupButton.onclick = handleEventButtonsAddGroup;

    const deleteButton = document.createElement("button");
    deleteButton.textContent = "🗑️";
    // deleteButton.onclick = () => container.removeChild(groupItem);
    deleteButton.addEventListener("click", function () {
        let choice = confirm(
            "Xác nhận xóa nhóm thông tin " + groupTitle.textContent + "?"
        );
        if (choice == true) {
            container.removeChild(groupItem);
        }
    });

    groupHeader.appendChild(groupTitle);
    groupHeader.appendChild(addInfoButton);
    groupHeader.appendChild(addGroupButton);
    groupHeader.appendChild(deleteButton);

    groupItem.appendChild(groupHeader);
    container.appendChild(groupItem);
    groupCounter++;
};

const exportPDF = () => {
    const printedInfo = document.getElementById("mainContentExport");
    console.log(printedInfo);
    html2canvas(printedInfo, {
        foreignObjectRendering: false,
        allowTaint: true,
        useCORS: true,
    }).then((canvas) => {
        // Use pdfMake to create a new PDF document
        const dataUrl = canvas.toDataURL();
        // console.log(dataUrl);

        const docDefinition = {
            content: [
                {
                    image: dataUrl,
                    width: 500,
                },
            ],
        };
        pdfMake.createPdf(docDefinition).download("ctt_premium.pdf"); //replace download("ctt_premium.pdf") by open() to preview;
    });
};

const handleEventGroupTitle = (groupTitle) => {
    const inputElement = document.createElement("input");
    inputElement.value = groupTitle.innerHTML;
    groupTitle.replaceWith(inputElement);

    // Add event listener for keydown
    inputElement.addEventListener("keydown", (event) => {
        // Check if the Enter key was pressed
        if (event.key === "Enter") {
            // Replace the input element with a span element
            const newgroupTitle = groupTitle;
            newgroupTitle.innerHTML = inputElement.value + "_20215182";
            inputElement.replaceWith(newgroupTitle);
        }
    });
};

const handleEventInfoItem = (label) => {
    const inputElement = document.createElement("input");
    inputElement.value = label.textContent;
    label.replaceWith(inputElement);

    // Add event listener for keydown
    inputElement.addEventListener("keydown", (event) => {
        // Check if the Enter key was pressed
        if (event.key === "Enter") {
            // Replace the input element with a span element
            const newLabel = label;
            newLabel.textContent = inputElement.value;
            newLabel.style = "width: 30%;";
            inputElement.replaceWith(newLabel);
        }
    });
};
